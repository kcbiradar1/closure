const limitFunctionCallCount = require("../limitFunctionCallCount.cjs");

function callback() {
  return `callback function executed successfully.`;
}

const result = limitFunctionCallCount(callback, 3);

for (let count = 0; count < 5; count++) {
  console.log(result());
}
