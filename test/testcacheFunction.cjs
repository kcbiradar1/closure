const cacheFunction = require('../cacheFunction.cjs');

function squareNumber(number) {
    return number * number;
}

const function_result = cacheFunction(squareNumber);

const number_array = [1,2,2,4,5,4];

for(let number of number_array) {
    const result = function_result(number);
    console.log(result);
}
