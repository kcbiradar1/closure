function limitFunctionCallCount(cb , n) {
    if(typeof cb === "function") {
        function invoke_callback() {
            if(n > 0) {
                n = n - 1;
                return cb();
            } else {
                return null;
            }
        }
        return invoke_callback;
    } else {
        return `Please send callback function properly.`;
    }
}

module.exports = limitFunctionCallCount;
