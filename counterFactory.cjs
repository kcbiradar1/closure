function counterFactory() {
  let counter = 10;
  function increment() {
    return counter + 1;
  }
  function decrement() {
    return counter - 1;
  }

  return { increment, decrement };
}

module.exports = counterFactory;
