function cacheFunction(cb) {
  if (typeof cb === "function") {
    const cache = {};
    function invoke_callback(number) {
      if (cache[number]) {
        return cache;
      } else {
        cache[number] = cb(number);
      }
      return cache;
    }
    return invoke_callback;
  } else {
    return `Please give the proper callback function.`;
  }
}
module.exports = cacheFunction;
